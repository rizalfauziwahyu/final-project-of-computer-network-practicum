using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleServerApplication
{
    class TcpSetUp
    {
        private TcpListener server = new TcpListener(IPAddress.Any, 9933);
        public static Hashtable threadHolder = new Hashtable();
        public static Hashtable socketHolder = new Hashtable();
        public static Hashtable userHolder = new Hashtable();
        public static IList<string> userInfo = new List<string>();
        private bool waitingClientConnection = true;
        private TcpClient clientSocket = new TcpClient();
        private static ushort connectId;
        const ushort MAXCONNECTED = 1000;
        
        public void StartServer()
        {
            Console.WriteLine("Listen at {0}", server.LocalEndpoint.ToString());
            server.Start();
            while(waitingClientConnection) 
            {
                try 
                {
                    Console.WriteLine("Waiting Client Connection");
                    clientSocket = server.AcceptTcpClient();
                    connectId++;
                    WaitingForClient();
                } 
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    server.Stop();
                }
            }
        }

        public void WaitingForClient()
        {
			bool isReachMaxClient = connectId > MAXCONNECTED;
			bool isSocketHolderContainsClientId = socketHolder.Contains(connectId);
			bool isSocketHolderFree = socketHolder.Count < MAXCONNECTED; 

            if (isReachMaxClient)
				connectId = 1;

			if (isSocketHolderFree)
			{
				if (isSocketHolderContainsClientId)
                {
					Console.WriteLine("Socket Holder Contains Client ID");
					Console.WriteLine("Client already connected");
				}
				// it is used to keep connected Sockets
				if (clientSocket.Connected)
					Console.WriteLine("Client-{0} Connected", connectId);
                CoreGame startGame = new CoreGame();
                startGame.startThread(connectId, clientSocket);
			}
        }
        
        public static void Main(String[] args)
        {
            TcpSetUp server = new TcpSetUp();
            server.StartServer();
        }

    }
    class CoreGame
    {
        ushort connectId;
        TcpClient clientSocket = null;
        public void startThread(ushort connectId, TcpClient clientSocket)
        {
            this.connectId = connectId;
            this.clientSocket = clientSocket;
            Thread tcpThd = new Thread(new ThreadStart(ReadSocket));
            tcpThd.Start();
        }
        
        public void ReadSocket()
        {
            NetworkStream stream = null;
            string temp;
            int signal;
            if (clientSocket.Connected)
            {
                Byte[] receive = new Byte[100000000];

                try 
                {
                    Console.WriteLine("waiting receive username client");
                    // add username to userInfo.List
                    stream = clientSocket.GetStream();
                    stream.Read(receive, 0, (int)clientSocket.ReceiveBufferSize);
                    temp = System.Text.Encoding.ASCII.GetString(receive);
                    temp = connectId.ToString() + "." + temp;
                    TcpSetUp.userInfo.Add(temp);
                    Array.Clear(receive, 0, receive.Length);
                    stream.Flush();

                    
                    Console.WriteLine("username already saved\nWaiting receive signal");
                    // wait signal from client
                    
                    stream.Read(receive, 0, (int)clientSocket.ReceiveBufferSize);
                    temp = System.Text.Encoding.ASCII.GetString(receive);
                    stream.Flush();
                    Console.WriteLine("{0}", temp);
                    Console.WriteLine("Signal already received");
                    Console.WriteLine("Ready to run signal");
                    signal = Convert.ToInt16(temp);
                    Console.WriteLine("instance signal created");

                    // signal to create lobby
                    if (signal == 5599)
                    {
                        Console.WriteLine("Signal Accepted : {0}", signal);
                        GameRoom misteriousRoom = new GameRoom();
                        Console.WriteLine("Ready to gameStart");
                        misteriousRoom.gameStart(connectId, clientSocket);
                        Console.WriteLine("Game have finised");
                    }

                    if (signal == 5588)
                    {
                        Console.WriteLine("waiting for invitation");
                        Console.WriteLine("add to socket holder");
                        TcpSetUp.socketHolder.Add(connectId, this.clientSocket);
                    }

                }
                catch (Exception e)
                {
                    if (!clientSocket.Connected)
                    {
                        Console.WriteLine(e.ToString());
                    }
                }
            }
            else
            {
                Console.WriteLine("Lost Connection");
            }
            Console.WriteLine("Thread finished");
            
        }
    }

    class GameRoom
    {
        private int playerScore;
        private int opponentScore;
        private bool playerWin;
        private bool opponentWin;
        private TcpClient playerSocket = null;
        private TcpClient opponentSocket = null;
	    private Socket _socketPlayer = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        private Socket _socketOpponent = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        private const int bufSize = 8 * 1024;
	    private State state = new State();
        private IPAddress playerAddress = null;
        private IPAddress opponentAddress = null;
        private int connectId;
        private int opponentConnectId;
        private short menu = 0;
        private int port = 9944;
        private bool receivingSignal = true;

        public class State
        {
            public byte[] buffer = new byte[bufSize];
        }

        public void ConnectToPlayer()
        {
            _socketPlayer.Connect(playerAddress, port);
        }
        public void ConnectToOpponent()
        {
            _socketOpponent.Connect(opponentAddress, port);
        }
        public void SendToPlayer(string text)
        {
            byte[] data = Encoding.ASCII.GetBytes(text);
            _socketPlayer.BeginSend(data, 0, data.Length, SocketFlags.None, (ar) => {
                    State so = (State)ar.AsyncState;
                    int bytes = _socketPlayer.EndSend(ar);
                    Console.WriteLine("SEND : {0}", text);		
                }, state);
        }
        public void SendToOpponent(string text)
        {
            byte[] data = Encoding.ASCII.GetBytes(text);
            _socketOpponent.BeginSend(data, 0, data.Length, SocketFlags.None, (ar) => {
                    State so = (State)ar.AsyncState;
                    int bytes = _socketOpponent.EndSend(ar);
                    Console.WriteLine("SEND : {0}", text);		
                }, state);
        }


        public void gameStart(int connectId, TcpClient clientSocket)
        {
            this.connectId = connectId;
            playerSocket = clientSocket;
            playerAddress = ((IPEndPoint)playerSocket.Client.RemoteEndPoint).Address;         
            displayUserList();

            while (receivingSignal)
            {
                receiveSignal(playerSocket);   
                if (this.menu == 5588)
                    gameStart(this.connectId, playerSocket);   

                if (this.menu != 0 && this.menu != 5588)
                {
                    Console.WriteLine("invite opponent");
                    inviteOpponent(this.menu);
                    receivingSignal = false;
                }
            }
            gamePlay();
        }
        public void inviteOpponent(int opponentConnectId)
        {
            this.opponentConnectId = opponentConnectId;
            Console.WriteLine("total socket : {0}", TcpSetUp.socketHolder.Count);
            
            foreach (var key in TcpSetUp.socketHolder.Keys)
            {
                Console.WriteLine("Key:{0}, Value:{1}",key , TcpSetUp.socketHolder[key]);
                // if ((object)opponentConnectId == key)
                // {

                    if (TcpSetUp.socketHolder.ContainsKey(key)) 
                    {
                        Console.WriteLine("contains connect id");
                        opponentSocket = (TcpClient) TcpSetUp.socketHolder[key];

                        if (opponentSocket.Connected)
                        {
                            Console.WriteLine("connected");
                            opponentAddress = ((IPEndPoint)opponentSocket.Client.RemoteEndPoint).Address;
                            
                            Console.WriteLine( "Opponent join to the room with IP {0}", ((IPEndPoint)opponentSocket.Client.RemoteEndPoint).Address );
                            gamePlay();
                        }
                        else
                        {
                            Console.WriteLine("not connected");    
                        }
                    }
                    else 
                    {
                        Console.WriteLine("not containts");
                    }
                //}
            }    
        }

        public void gamePlay()
        {
            //
        }

        public void receiveSignal(TcpClient clientSocket)
        {
            Console.WriteLine("Start receiving");
            string temp;
            Byte[] receive = new Byte[10000000];
            NetworkStream stream = clientSocket.GetStream();
            stream.Read(receive, 0, (int) clientSocket.ReceiveBufferSize);
            temp = System.Text.Encoding.ASCII.GetString(receive);
            Console.WriteLine("REC : {0}", temp);
            
            this.menu = Convert.ToInt16(temp);
        }

        public int RandomNumber(int min, int max)    
        {    
            Random random = new Random();    
            return random.Next(min, max);    
        }

        public void sendMessage(TcpClient clientSocket, string message)
        {
            byte[] sendBuffer = System.Text.Encoding.ASCII.GetBytes(message);
            NetworkStream stream = clientSocket.GetStream();
            Console.WriteLine("Start send {0}", message);
            stream.Write(sendBuffer, 0, (int) sendBuffer.Length);
            stream.Flush();

            Console.WriteLine("Already send message");
        }

        public string getUserList()
        {
            string usersList = String.Empty;
            for (int i = 0; i < TcpSetUp.userInfo.Count; i++)
            {
                usersList = String.Concat(usersList, String.Concat(TcpSetUp.userInfo[i], " "));
            }
            return usersList;
        }

        public void displayUserList()
        {
            Console.WriteLine("Ready to send user list");
            Console.WriteLine("connectId is {0}", connectId);

             if (playerSocket.Connected)
             {
                Console.WriteLine("ready to send");
                string pesan = getUserList();
                sendMessage(playerSocket, pesan);
                Thread.Sleep(2000);
                sendMessage(playerSocket, "finish");
                Thread.Sleep(1000);
                sendMessage(playerSocket, "5577");
             }
             else
                Console.WriteLine("not connected");
        }
    }
}