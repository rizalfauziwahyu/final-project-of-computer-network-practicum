using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections;

namespace HandleConnection
{
	class TcpSetUp
	{
		public static void Main(String[] args)
		{
			// Set the Tcp Listener on port 9933
			short port = 9933;
			bool waitingClientConnection = true;			
			TcpListener server = new TcpListener(IPAddress.Any, port);
			TcpClient socketClient = new TcpClient();

			Console.WriteLine("Listen at {0}:{1}", server.LocalEndpoint.ToString(), port);
			// Start listening client request
			server.Start();
			while(waitingClientConnection) {
				Console.WriteLine("Waiting Client Connection");
				socketClient = server.AcceptTcpClient();
				// instance class ThreadHandler
				ThreadHandler threadHandler = new ThreadHandler();
				threadHandler.StartThreadHolder(socketClient);
			}
		}

	}

	class ThreadHandler
	{
		TcpClient clientSocket = new TcpClient();
		Hashtable socketHolder = new Hashtable();
		Hashtable threadHolder = new Hashtable();
		ushort connectId = 0;
		const ushort MAXCONNECTED = 1000;
		
		public void StartThreadHolder(TcpClient clientSocket){
			Thread tcpThd = new Thread(new ThreadStart(WaitingForClient));
			threadHolder.Add(connectId, tcpThd);
			tcpThd.Start();
		}
		public void WaitingForClient()
		{
			bool isNotReachMaxClient = connectId < MAXCONNECTED;
			bool isSocketHolderContainsClientId = socketHolder.Contains(connectId);
			bool isSocketHolderFull = socketHolder.Count < MAXCONNECTED;
			while(true)
			{
				connectId++;
				Console.WriteLine("Client-{0} : started", connectId);
				
				if (isNotReachMaxClient)
					connectId++;
				else
					connectId = 1;
				
				if (isSocketHolderFull)
				{
					while (isSocketHolderContainsClientId)
						connectId++;
					// it is used to keep connected Sockets
					socketHolder.Add(connectId, clientSocket);
					Thread td = new Thread(new ThreadStart(ReadSocket));
					// it is used to keep the active thread
					threadHolder.Add(connectId, td);
					td.Start();
				}
			}
		}

// follow function handle the communication from the clients and close the    
// socket and the thread when the socket connection is down   
		public void ReadSocket()
		{
			
		}
	}
 }
