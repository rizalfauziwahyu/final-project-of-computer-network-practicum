using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.IO;
using System.Threading;
using System.Collections;
using System.Collections.Generic;

namespace ClientConsole
{
    class GameMenu
    {
        public static string playerNickname = null;
        public static TcpClient client = new TcpClient("127.0.0.1", 9933);
        public void MainMenu()
        {
            // set user nickname
            if (GameMenu.playerNickname == null)
                SetNickname();
            short menu = 0;
            while (menu != 3)
            {
                Console.WriteLine("Welcome to BinaryTree Race Game");
                Console.Write("Game Menu :\n");
                Console.Write("1. Play Game\n");
                Console.Write("2. About\n");
                Console.Write("3. Exit\n");
                Console.Write("Input Menu : ");
                string inputMenu;
                inputMenu = Console.ReadLine();
                // Validate user input 
                while(!Int16.TryParse(inputMenu, out menu))
                {
                    Console.WriteLine("Not a valid number, try again.");
                    inputMenu = Console.ReadLine();
                }
                menu = Convert.ToInt16(inputMenu);
                Console.Clear();
                switch(menu)
                {
                    case 1:
                        GoToMenu();
                        break;
                    case 2:
                        Console.WriteLine("you choose 2");
                        break;
                }
            }
        }

        public void SetNickname()
        {
            byte[] send;
            string nickname;
            NetworkStream stream = client.GetStream();
            Console.Write("Input Your Nickname : ");
            nickname = Console.ReadLine();
            GameMenu.playerNickname = nickname;
            send = System.Text.Encoding.ASCII.GetBytes(nickname);
            stream.Write(send, 0, (int) send.Length);
            stream.Flush();
            //stream.EndWrite();
            //stream.Close();
            Console.Clear();
        }
        public void sendSignalCreateLobby()
        {
            NetworkStream stream = client.GetStream();
            Byte[] signalCreateLobby = new Byte[10000000];
            signalCreateLobby = System.Text.Encoding.ASCII.GetBytes("5599");       
            stream.Write(signalCreateLobby, 0, (int) signalCreateLobby.Length);
            stream.Flush();
            //stream.Close();
        }
        public void sendSignalWaitingInvitation()
        {
            NetworkStream stream = client.GetStream();
            Byte[] signalCreateLobby = new Byte[10000000];
            signalCreateLobby = System.Text.Encoding.ASCII.GetBytes("5588");       
            stream.Write(signalCreateLobby, 0, (int) signalCreateLobby.Length);
            stream.Flush();
            //stream.Close();
        }
        
        public void GoToMenu()
        {
            Console.WriteLine("Player : {0}", GameMenu.playerNickname);
            short menu;
            string inputMenu;
            Console.Write("1. Create Lobby\n");
            Console.Write("2. Waiting Invitation\n");
            Console.Write("Input menu : ");
            inputMenu = Console.ReadLine();
            // Validate user input 
            while(!Int16.TryParse(inputMenu, out menu))
            {
                Console.WriteLine("Not a valid number, try again.");
                inputMenu = Console.ReadLine();
            }
            menu = Convert.ToInt16(inputMenu);
            Console.Clear();
            if (menu == 1)
                CreateLobby();
            if (menu == 2)
                WaitingInvitation();
        }


        public void CreateLobby()
        {
             sendSignalCreateLobby();
             GameLobby newLobby = new GameLobby();
             newLobby.startThread();
        }

        public void WaitingInvitation()
        {
            sendSignalWaitingInvitation();
            Console.WriteLine("waiting invitation...");
        }


        public static void Main(String[] args)
        {
            GameMenu startGame = new GameMenu();
            startGame.MainMenu();
        }
    }

    public class GameLobby
    {
        TcpClient clientSocket = GameMenu.client;

        public void startThread()
        {
            Thread infLobby = new Thread(new ThreadStart(infoLobby));
            infLobby.Start();

            Console.WriteLine("Wait until appear word finish");
            Console.WriteLine("5588. refresh list player on server");
            
            short menu;
            string inputMenu;
            inputMenu = Console.ReadLine();
            // Validate user input 
            while(!Int16.TryParse(inputMenu, out menu))
            {
                Console.WriteLine("Not a valid number, try again.");
                inputMenu = Console.ReadLine();
            }

            menu = Convert.ToInt16(inputMenu);
            Console.Clear();
            
            if (menu == 5588)
            {
                sendMessage(clientSocket, inputMenu);
                startThread();
            }
            else
            {
                sendMessage(clientSocket, inputMenu);
                // gameplay
                GamePlay startGame = new GamePlay();
                startGame.beginGamePlay();
            }
        }

        public void sendMessage(TcpClient clientSocket, string message)
        {
            byte[] sendBuffer = System.Text.Encoding.ASCII.GetBytes(message);
            //sendBuffer = System.Text.Encoding.ASCII.GetBytes(message);
            NetworkStream stream = clientSocket.GetStream();
            Console.WriteLine("Start send {0}", message);
            stream.Write(sendBuffer, 0, (int) sendBuffer.Length);
            stream.Flush();

            Console.WriteLine("Already send message");
        }

        public void infoLobby()
        {
            bool receiveUserList = true;
            int signal = 0;
            Byte[] receive = new Byte[100000000];
            string temp = string.Empty;
            NetworkStream stream = clientSocket.GetStream();
            stream.Flush();

            Console.WriteLine("\nReceiving list users on server\nwait for a minute");
            while (receiveUserList)
            {
                if (clientSocket.Connected)
                {
                    stream.Read(receive, 0, (int) clientSocket.ReceiveBufferSize);
                
                    stream.Flush();
                    //Console.WriteLine("already receive message");
                    temp = System.Text.Encoding.ASCII.GetString(receive);

                    if(Int32.TryParse(temp, out signal))
                    {
                        signal = Convert.ToInt32(temp);
                    }

                    if (signal == 5577)
                    {
                        Console.WriteLine("finish");
                        Console.Write("Choose number on player username to invite :\n");
                        Array.Clear(receive, 0, (int) receive.Length);
                        temp = " ";
                        receiveUserList = false;
                    }

                    if (temp.IndexOf("a", StringComparison.CurrentCultureIgnoreCase) > -1) {
                        Console.WriteLine(temp);
                    }
                    temp = string.Empty;
                    Array.Clear(receive, 0, (int) receive.Length);
                }
            }   
        }
    }

    class GamePlay
    {
        private int playerScore = 0;
        private int opponentScore = 0;
        private bool playerWin = false;
        private bool opponentWin = false;
        private IList<int> dataToInsertOnBinaryTree = new List<int>();
        private Tree theTree = new Tree();
	    private Socket _socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
	    private EndPoint epFrom = new IPEndPoint(IPAddress.Any, 0);
        private const int bufSize = 8 * 1024;
        private AsyncCallback recv = null;
        private State state = new State();
        private Random random = new Random();


        private class State
        {
            public byte[] buffer = new byte[bufSize];
        }
        public void Server(string address, int port)
        {
            _socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.ReuseAddress, true);
            _socket.Bind(new IPEndPoint(IPAddress.Parse(address), port));
            Receive();
        }
        public void Receive()
        {
            _socket.BeginReceiveFrom(state.buffer, 0, bufSize, SocketFlags.None, ref epFrom, recv = (ar) => {
                State so = (State)ar.AsyncState;
                int bytes = _socket.EndReceiveFrom(ar, ref epFrom);
                _socket.BeginReceiveFrom(so.buffer, 0, bufSize, SocketFlags.None, ref epFrom, recv, so);
			    string[] splitArgument = Encoding.ASCII.GetString(so.buffer, 0, bytes).Split(" ");
                playerScore = Convert.ToInt32(splitArgument[0]);
                opponentScore = Convert.ToInt32(splitArgument[1]);
                Console.WriteLine("RECV player score : {0}", playerScore);
                Console.WriteLine("RECV opponent score: {0}", playerScore);
            }, state);
        }

        public void SendScoreToServer(string score)
        {
            NetworkStream stream = GameMenu.client.GetStream();
            Byte[] send = System.Text.Encoding.ASCII.GetBytes(score);       
            stream.Write(send, 0, (int) send.Length);
            stream.Flush();
        }

        public void WinLoseCondition()
        {
            Console.WriteLine("Game Over");
            if (playerWin)
                Console.WriteLine("You Win!!!");
            if (opponentWin)
                Console.WriteLine("You Lose!!!");
            Console.WriteLine("{0} : {1}", GameMenu.playerNickname, playerScore);
            Console.WriteLine("Opponent : {0}", opponentScore);
        }

        public void ReceiveSignalWinOrLose()
        {
            Console.WriteLine("Start receiving signal win or lose");
            string temp;
            short signal;
            Byte[] receive = new Byte[1024];
            NetworkStream stream = GameMenu.client.GetStream();
            stream.Read(receive, 0, (int) GameMenu.client.ReceiveBufferSize);
            temp = System.Text.Encoding.ASCII.GetString(receive);
            Console.WriteLine("REC : {0}", temp);
            signal = Convert.ToInt16(temp);
            
            if (signal == 1122)
                playerWin = true;
            else if (signal == 1133)
                opponentWin = true;
        }

        public void SendSignalGameFinish()
        {
            NetworkStream stream = GameMenu.client.GetStream();
            Byte[] signalCreateLobby = System.Text.Encoding.ASCII.GetBytes("1111");       
            stream.Write(signalCreateLobby, 0, (int) signalCreateLobby.Length);
            stream.Flush();
        }
        public void DisplayScore()
        {
            while (!(playerWin || opponentWin))
            {
                Thread.Sleep(100);
                Console.WriteLine("Score\n{0} : {1}\nOpponent : {2}", GameMenu.playerNickname, playerScore, opponentScore);
                Console.Clear();
            }
        }

        public void AddDataForBinaryTree()
        {
            for (int i = 0; i < 20; i++)
            {
                dataToInsertOnBinaryTree.Add(RandomNumber(1,99));
            }
        }

        public int RandomNumber(int min, int max)    
        {        
            return random.Next(min, max);    
        }

        public void beginGamePlay()
        {
            Server("127.0.0.1", 9944);
            AddDataForBinaryTree();
            int parent = dataToInsertOnBinaryTree[0];

            Thread setWinner = new Thread (new ThreadStart(ReceiveSignalWinOrLose));
            setWinner.Start();

            Thread displayPlayerScore = new Thread (new ThreadStart(DisplayScore));
            displayPlayerScore.Start();

            for (int i = 1; i < dataToInsertOnBinaryTree.Count; i++)
            {
                Console.WriteLine("Press 'a' -> left and 'd' -> right ");
                Console.WriteLine("Root / parent of your tree is {0}", parent);
                Console.WriteLine("The next number is {0}", dataToInsertOnBinaryTree[i]);
                Console.WriteLine("left or right ? (a/d)");
                string input;
                input = Console.ReadLine();
                // Validate user input 
                while(  !(String.Equals("a", input, StringComparison.OrdinalIgnoreCase) || String.Equals("d", input, StringComparison.OrdinalIgnoreCase)))
                {
                    Console.WriteLine("Not a valid input, Plese use 'a' or 'd'.");
                    input = Console.ReadLine();
                }

                if (String.Equals("a", input, StringComparison.OrdinalIgnoreCase))
                {
                    if (parent > dataToInsertOnBinaryTree[i])
                    {
                        SendScoreToServer(10.ToString());
                    }
                    else
                    {
                        SendScoreToServer(1.ToString());
                    }
                }
                else if (String.Equals("d", input, StringComparison.OrdinalIgnoreCase))
                {
                    if (parent < dataToInsertOnBinaryTree[i])
                    {
                        SendScoreToServer(10.ToString());
                    }
                    else
                    {
                        SendScoreToServer(1.ToString());
                    }
                }

                theTree.Insert(dataToInsertOnBinaryTree[i]);
                parent = dataToInsertOnBinaryTree[i];

                if (playerWin || opponentWin)
                { 
                    Console.Clear();
                    break;
                }
            }

            SendSignalGameFinish();

            WinLoseCondition();

            Console.WriteLine("Inorder Traversal Of Tree : ");
            theTree.Inorder(theTree.ReturnRoot());

            Console.WriteLine("Press any key to go back on main menu");
            Console.ReadLine();
        }

    }

    class Node
    {
        public int item;
        public Node leftc;
        public Node rightc;
        public void display()
        {
            Console.Write("[");
            Console.Write(item);
            Console.Write("]");
        }
    }

    class Tree
    {
        public Node root;
        public Tree()
        { 
            root = null; 
        }
        public Node ReturnRoot()
        {
            return root;
        }
        public void Insert(int id)
        {
            Node newNode = new Node();
            newNode.item = id;

            if (root == null)
                root = newNode;
            else
            {
                Node current = root;
                Node parent;

                while (true)
                {
                    parent = current;

                    if (id < current.item)
                    {
                        current = current.leftc;

                        if (current == null)
                        {
                            parent.leftc = newNode;
                            return;
                        }
                    }
                    else
                    {
                        current = current.rightc;

                        if (current == null)
                        {
                            parent.rightc = newNode;
                            return;
                        }
                    }
                }
            }

        }

        public void Inorder(Node Root)
        {
            if (Root != null)
            {
                Inorder(Root.leftc);
                Console.Write(Root.item + " ");
                Inorder(Root.rightc);
            }
        }
    }
}